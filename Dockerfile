FROM python:3.9-alpine
RUN apk add git \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    py3-pillow \
    mariadb-dev \
    # for jpeg
    jpeg-dev \
    zlib-dev \
    freetype-dev \
    lcms2-dev \
    openjpeg-dev \
    libffi-dev \
    tiff-dev \
    tk-dev \
    tcl-dev
RUN pip install 'cryptography<=3'
RUN pip --no-cache-dir install poetry
WORKDIR /app/
ADD pyproject.toml poetry.lock poetry.toml ./
RUN poetry install 
ADD ./ ./
ENTRYPOINT /app/.venv/bin/python manage.py runserver 0.0.0.0:8080
EXPOSE 8080