TAG=registry.gitlab.com/qzibet/sunrise-mini-online-store
CADDY_TAG=registry.gitlab.com/qzibet/sunrise-mini-online-store/caddy

build_django:
		docker build -t ${TAG} .

build_caddy:
		docker build -f caddy.Dockerfile -t ${CADDY_TAG} .

publish_django:
		docker push ${TAG}  

publish_caddy:
		docker push ${CADDY_TAG}

# deploy:
# 		docker stack deploy -c ./docker-compose.yml -c ./docker-compose.prod.yml --with-registry-auth 


