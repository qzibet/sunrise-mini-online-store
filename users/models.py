from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class Profile(AbstractUser):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        default=None,
        null=True,
        on_delete=models.CASCADE,
    )
    phone = models.CharField(
        max_length=20,
        verbose_name="Номер телефона",
        null=True,
        blank=True,
    )
    address = models.CharField(
        max_length=255,
        verbose_name="Адрес",
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
