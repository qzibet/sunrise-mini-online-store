from django.urls import path, re_path
from .views import (
    PostList,
    show_category,
    PostListView,
    PostCRUDView,
)

urlpatterns = [
    path("", PostList.as_view()),
    re_path(r"^category/(?P<hierarchy>.+)/$", show_category, name="category"),
    path(
        "api/v1/post/<slug:slug>/",
        PostCRUDView.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "delete": "destroy",
            }
        ),
    ),
    path(
        "api/v1/post/",
        PostListView.as_view(),
    ),
]
