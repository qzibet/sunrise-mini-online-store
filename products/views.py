from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.views.generic import View
from .models import (
    Category,
    Post,
)
from .serializers import (
    PostListSerializer,
    PostSerializer,
)
from rest_framework import permissions, generics
from .classes import CreateRetrieveUpdateDestroy


class PostList(View):
    def get(self, request: HttpResponse):
        category = Category.objects.all()
        post = Post.objects.all()

        return render(
            request,
            "products/index.html",
            {"category": category, "post": post},
        )


def show_category(request, hierarchy=None):
    category_slug = hierarchy.split("/")
    print(category_slug)
    parent = None
    root = Category.objects.all()

    for slug in category_slug[:-1]:
        parent = root.get(parent=parent, slug=slug)

    try:
        instance = Category.objects.get(parent=parent, slug=category_slug[-1])
    except Exception:
        instance = get_object_or_404(Post, slug=category_slug[-1])
        return render(
            request,
            "products/post_detail.html",
            {
                "instance": instance,
            },
        )
    else:
        return render(
            request,
            "products/categories.html",
            {
                "instance": instance,
            },
        )


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    filter_fields = ("category__slug",)


class PostCRUDView(CreateRetrieveUpdateDestroy):

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    lookup_field = "slug"
    permission_classes_by_action = {
        "get": [permissions.AllowAny],
        "update": [permissions.AllowAny],
        "destroy": [permissions.AllowAny],
    }

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
