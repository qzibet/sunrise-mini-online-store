from django.db import models
from django.db.models.deletion import CASCADE
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey


class Post(models.Model):
    created_at = models.DateTimeField(
        "Дата добавления",
        auto_now_add=True,
    )
    title = models.CharField(
        "Название товара",
        max_length=150,
    )
    slug = models.SlugField(
        "Слаг",
        unique=True,
        editable=True,
    )
    description = models.TextField(
        "Описание товара",
    )
    price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        verbose_name="Цена",
    )
    image = models.ImageField(
        "Фотография товара",
        upload_to="product-image/%Y/%m/%d",
    )
    category = TreeForeignKey(
        "Category",
        null=True,
        blank=True,
        on_delete=CASCADE,
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("products", args=[str(self.slug)])

    class Meta:
        ordering = ["-created_at"]
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class Category(MPTTModel):
    title = models.CharField("Название категории", max_length=150, unique=True)
    parent = TreeForeignKey(
        "self",
        null=True,
        blank=True,
        on_delete=CASCADE,
        related_name="children",
        db_index=True,
    )
    slug = models.SlugField("Слаг")

    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except Exception:
            ancestors = []
        else:
            ancestors = [i.slug for i in ancestors]
        slugs = []
        for i in range(len(ancestors)):
            slugs.append("/".join(ancestors[: i + 1]))
        return slugs

    def __str__(self):
        return self.title

    class MPTTMeta:
        order_insertion_by = ["title"]

    class Meta:
        unique_together = (
            "parent",
            "slug",
        )
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
