from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import (
    Category,
    Post,
)


@admin.register(Category)
class CategoryAdmin(DraggableMPTTAdmin):
    prepopulated_fields = {"slug": ("title",)}


@admin.register(Post)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
